//
//  TabletestVC.swift
//  AlamofireTest
//
//  Created by Srishti on 28/10/22.
//

import UIKit

class TabletestVC: UIViewController {

    @IBOutlet weak var Mtable: UITableView!
    var selectedarray = [String]()
    var tablearray = ["hi","hello","how are you","where ","are you","from ","thanks","iphone","android","mac","windows","mac mini","macbook pro"]
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    
    @IBAction func CheckButtonTapped(_ sender: UIButton) {
        print(sender.tag)
        
    }
    
   

}
extension TabletestVC
:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tablearray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Tcell", for: indexPath) as! Tcell
        cell.cellLBL.text = tablearray[indexPath.row]
        cell.checkBTN.tag = indexPath.row
        return cell
    }
    
    
}



class Tcell:UITableViewCell{
 
    @IBOutlet weak var cellLBL: UILabel!
    @IBOutlet weak var cellIMG: UIImageView!
    
    @IBOutlet weak var checkBTN: UIButton!
    
    
    
}
