//
//  dateconvertervc.swift
//  AlamofireTest
//
//  Created by Srishti on 12/12/22.
//
import UIKit
import Foundation
class initialVC:UIViewController{
    let dateFormat = "yyyy-MM-dd HH:mm:ss"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let formatter = ISO8601DateFormatter()
        // Insert .withFractionalSeconds to the current format.
        formatter.formatOptions.insert(.withFractionalSeconds)
        //let date = formatter.date(from: "2022-10-21T16:29:24.285Z")
        formatter.formatOptions = [
            .withInternetDateTime,
            .withFractionalSeconds
        ]
        let date2 = formatter.date(from: "2022-10-21T16:29:24.285Z")
       // print("date * :",date ?? "")
        print("date mod * :",date2 ?? "")
        let mdate = "2019-02-25 10:20:21"
        print(
            self.timeInterval(timeAgo: "\(mdate ?? "")" )
        )
    }
    
    
    
    
    func timeInterval(timeAgo:String) -> String
    {
        let df = DateFormatter()
        
        df.dateFormat = dateFormat
        let dateWithTime = df.date(from: timeAgo)
        
        let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: dateWithTime!, to: Date())
        
        if let year = interval.year, year > 0 {
            return year == 1 ? "\(year)" + " " + "year ago" : "\(year)" + " " + "years ago"
        } else if let month = interval.month, month > 0 {
            return month == 1 ? "\(month)" + " " + "month ago" : "\(month)" + " " + "months ago"
        } else if let day = interval.day, day > 0 {
            return day == 1 ? "\(day)" + " " + "day ago" : "\(day)" + " " + "days ago"
        }else if let hour = interval.hour, hour > 0 {
            return hour == 1 ? "\(hour)" + " " + "hour ago" : "\(hour)" + " " + "hours ago"
        }else if let minute = interval.minute, minute > 0 {
            return minute == 1 ? "\(minute)" + " " + "minute ago" : "\(minute)" + " " + "minutes ago"
        }else if let second = interval.second, second > 0 {
            return second == 1 ? "\(second)" + " " + "second ago" : "\(second)" + " " + "seconds ago"
        } else {
            return "a moment ago"
            
        }
    }
}
