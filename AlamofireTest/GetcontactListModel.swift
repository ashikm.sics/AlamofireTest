//
//  GetcontactListModel.swift
//  AlamofireTest
//
//  Created by Srishti on 26/11/22.
//

import Foundation

class getcontactResponse{
    var status : Bool?
    var Data : [getcontactDataModel] = []
    init(from data : [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        if let dataarray = data["data"] as? [[String:Any]]{
            for contactlist in dataarray{
                self.Data.append(getcontactDataModel(from: contactlist))
            }
        }
        
    }
}
class getcontactDataModel{
    var Headername : String?
    var contactsList : [contactListModel] = []
    init(from data : [String:Any]) {
        self.Headername = data["Headername"] as? String
        if let contactlist = data["contactsList"] as? [[String:Any]]{
            for contact in contactlist{
                self.contactsList.append(contactListModel(from: contact))
            }
        }
    }
}
class contactListModel{
   var  id :String?
   var  categoryIds : [String]?
   var  contactUserId :String?
   var  isRecommended :Int?
   var  isReferred :Int?
   var  isUser :Int?
   var  name :String?
   var  phoneNumber :String?
   var  twilioCallerName :String?
   var  twilioCarrierType :String?
   var  type :String?
   var  userId :String?

    
    init(from data : [String:Any]) {
        self.id = data["_id"] as? String
        self.categoryIds = data["categoryIds"] as? [String]
        self.contactUserId = data["contactUserId"] as? String
        self.isRecommended = data["isRecommended"] as? Int
        self.isReferred = data["isReferred"] as? Int
        self.isUser = data["isUser"] as? Int
        self.name = data["name"] as? String
        self.phoneNumber = data["phoneNumber"] as? String
        self.twilioCallerName = data["twilioCallerName"] as? String
        self.twilioCarrierType = data["twilioCarrierType"] as? String
        self.type = data["type"] as? String
        self.userId = data["userId"] as? String
    }
}
