//
//  categoryVC.swift
//  AlamofireTest
//
//  Created by Srishti on 14/12/22.
//

import UIKit

class categoryVC: UIViewController {
let catarray = ["plumber","accountant","sweeper","ac me","carpender","driver"]
 var counter = 0
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    
    @IBOutlet weak var countlbl: UILabel!
    @IBOutlet weak var stakV: UIStackView!
    @IBOutlet weak var stackheight: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        counter = 0
        countlbl.text = "\(counter)"
        setdata(countvalue: counter)
        
        
    }
    
    @IBAction func minusBTn(_ sender: UIButton) {
        if counter == 0{
            
        }else{
            counter = counter - 1
            countlbl.text = "\(counter)"
            setdata(countvalue: counter)
        }
       
    }
    

    @IBAction func plusBTn(_ sender: UIButton) {
        counter = counter + 1
        countlbl.text = "\(counter)"
        setdata(countvalue: counter)
    }
    
    
    
    func setdata(countvalue:Int){
        print("count = ",countvalue)
        if countvalue >= 3{
            stackheight.constant = 50 * 3
            lbl1.text = catarray[0]
            lbl2.text = catarray[1]
            lbl3.text = catarray[2]
            lbl1.isHidden = false
            lbl2.isHidden = false
            lbl3.isHidden = false
        }
        else if countvalue == 2{
            stackheight.constant = 50 * 2
            lbl1.text = catarray[0]
            lbl2.text = catarray[1]
            lbl1.isHidden = false
            lbl2.isHidden = false
            lbl3.isHidden = true
        }
        else if countvalue == 1{
            stackheight.constant = 50 * 1
            lbl1.text = catarray[0]
            lbl2.isHidden = true
            lbl3.isHidden = true
            
        }
        else {
           
            stackheight.constant = 0
            
        }
    }
}
