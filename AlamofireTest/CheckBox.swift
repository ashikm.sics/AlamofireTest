//
//  CheckBox.swift
//  AlamofireTest
//
//  Created by Srishti on 28/10/22.
//

import UIKit

class zzz: UIViewController, UITableViewDelegate, UITableViewDataSource {

var selectedRow:[IndexPath] = []
var tablearray = ["hi","hello","how are you","where ","are you","from ","thanks","iphone","android","mac","windows","mac mini","macbook pro"]
    
@IBOutlet var tblView:UITableView!

override func viewDidLoad()
{
    super.viewDidLoad()
}

func tableView(_ tableView: UITableView, numberOfRowsInSection section:
    Int) -> Int
{
    return 100
}
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
{
    var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")

    if cell == nil
    {
        cell = UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
    }

    cell?.textLabel?.text = String(format:"%@",indexPath.row)

    if selectedRow.contains(indexPath)
    {
        cell?.accessoryType = .checkmark
    }
    else
    {
        cell?.accessoryType = .none
    }
    return cell!
}
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
{
    if selectedRow.contains(indexPath)
    {
        // For  Remove Row selection
        selectedRow.remove(at: selectedRow.index(of: indexPath)!)
    }
    else
    {
        selectedRow.append(indexPath)
    }

    tableView.reloadData()
}

// Call this method for Select All
func selectAllCall()
{
    var indexPaths: [IndexPath] = []
    for j in 0..<100
    {
        indexPaths.append(IndexPath(row: j, section: 0))
    }

    selectedRow.removeAll()

    selectedRow.append(contentsOf: indexPaths)

    tblView.reloadData()
}
}

class Cell:UITableViewCell{
    
}
