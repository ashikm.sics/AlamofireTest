//
//  ViewController.swift
//  AlamofireTest
//
//  Created by Srishti on 20/10/22.
//

import UIKit
import Alamofire
import ContactsUI
import Contacts
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate {
    var code = "+91"
    var mob = "9895649997"
    let store = CNContactStore()
    var countryname = ""
    var contactlist = [getcontactDataModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        api()
        // SyncContact()
        //getContactList()
    }
    //MARK: - Location base-----------------
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let userLocation:CLLocation = locations[0] as CLLocation
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        
        
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }
            if let placemark = placemarks {
                if placemark.count>0 {
                    let placemark = placemarks?.first
                    print(placemark?.locality!)
                    print(placemark?.administrativeArea!)
                    print(placemark?.country!)
                    self.countryname = placemark?.country ?? "Is empty"
                    //  self.getcountryDelegate.getCountryName(name: self.countryName)
                    // return
                }
            }
        }
        
    }
    
    
    
    
    
    //MARK: - Contact sync base ------------------------------------------------------------------------------------------
    func getContactList(){
        let predicate = CNContact.predicateForContactsInContainer(withIdentifier:store.defaultContainerIdentifier())
        let contactz = try! store.unifiedContacts(matching: predicate, keysToFetch: [CNContactGivenNameKey as
                                                                                     CNKeyDescriptor, CNContactPhoneNumbersKey as CNKeyDescriptor])
        print("Total of ",contactz.count,"has been synced")
        for contact in contactz {
            print("Name :",contact.givenName)
            
            for ph in contact.phoneNumbers{
                print("Mob :",ph.value.stringValue)
            }
        }
        
    }
    //-------------------------------------------------------------------------------------------------------------------------------
    
    func SyncContact(){
        var contacts = [CNContact]()
        let keys = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName)]
        
        let request = CNContactFetchRequest(keysToFetch: keys)
        
        let contactStore = CNContactStore()
        do {
            try contactStore.enumerateContacts(with: request) {
                (contact, stop) in
                // Array containing all unified contacts from everywhere
                contacts.append(contact)
            }
            print("Congrats total of ",contacts.count,"synced")
            print(contacts)
        }
        catch {
            print("unable to fetch contacts")
            
        }
    }
    //MARK: - Login api
    
    func api(){
        let params = ["jobId":"639083f0d81b535c35571603"] as [String : Any]
        
        var url = "https://staging.extrahourz.com/api/app/employer/jobDetails"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["token":"e"]).validate(statusCode: 200..<500) .responseJSON { response in
            
            print(response)
            switch (response.result) {

                       case .success( let JSON):

                      
                if let responsedata =  JSON as? [String:Any]  {
//                               let result = getcontactResponse(from:responsedata)
//                               self.contactlist = result.Data
               
               
                           }
                        case .failure(let error):
                           print("Request error: \(error.localizedDescription)")
                    }
//
        }
    }
    
}



